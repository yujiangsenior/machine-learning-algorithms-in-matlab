%% 初始化
    clc
	clear
	close all
    isplot=1; %是否画出可视化图，1为画图，0为不画图;
    di = 1; %距离方案，1为欧拉距离，2为曼哈顿距离，3为夹角余弦;
    K = 15; %knn近邻个数;
    
% 红酒回归

    isclassify = 0; %isclassify=1为分类，isclassify=0为回归;
    data = load ('Wine.mat');
    testLabels = data.A(1001:1599,12);
    TestSet = data.A(1001:1599,1:11);
    trainLabels = data.A(1:1000,12);
    trainSet = data.A(1:1000,1:11);
    dimensions=size(trainSet,2);
    regression = KNN(TestSet,trainSet,trainLabels,dimensions,K, di, isplot, isclassify,testLabels);
    
%% 手写体分类

    isclassify = 1; %isclassify=1为分类，isclassify=0为回归;
    data = load ('Digits.mat');
    testLabels = data.A(1935:2880,1025);
    TestSet = data.A(1935:2880,1:1024);
    trainLabels = data.A(1:1934,1025);
    trainSet = data.A(1:1934,1:1024);
    dimensions=size(trainSet,2);
    classify = KNN(TestSet,trainSet,trainLabels,dimensions,K, di, isplot, isclassify,testLabels);

%% 鸢尾花分类

    isclassify = 1; %isclassify=1为分类，isclassify=0为回归;
    load fisheriris;
    idx=randsample(150,100);
    trainLabels=species(idx);
    testLabels=species(setdiff(1:length(species),idx));
    trainSet=meas(idx,:);
    TestSet =meas(setdiff(1:length(species),idx),:);
    dimensions=size(trainSet,2);
    classify = KNN(TestSet,trainSet,trainLabels,dimensions,K, di, isplot, isclassify,testLabels);
    