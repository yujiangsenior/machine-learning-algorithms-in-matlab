%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%函数名称：kenfisher_train()
%参数train：训练样本的特征
%返回值：km：训练得出的投影向量
%函数功能：基于核的Fisher分类器的训练函数
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
function km=kenfisher_train(train)
    
    km=cell(length(train),length(train));
    for i=2:length(train)
        for j=1:i-1
            km{i,j}=kenfisher_model(train{i},train{j});%train{i},train{j}分别为train这个10x1 cell中的一个cell，代表一类标签的训练数据
        end
    end
end