%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%函数名称：kernel_function()
%参数x1, x2：两个样本数据，可以是测试数据，也可以是训练数据，一般都是d维向量（d是所有数据共同的维度）
%返回值：y：核函数
%函数功能：基于核的Fisher分类器的核函数
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function y=kernel_function(x1,x2)

    y=(x1'*x2+1)^2;
end