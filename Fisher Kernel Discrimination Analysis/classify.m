%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%函数名称：classify()
%参数：obj：训练得出的km中的某一个元素km{i,j}; sample：待分类样本
%返回值：classfit：返回两类标签中的更接近于待分类样本的标签，1或者2，1代表属于km{i,j}中的i类，2代表属于km{i,j}中j类
%函数功能：基于核的Fisher分类器的预测函数2

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function classfit=classify(obj,sample)
    y=0;
    sample=reshape(sample,length(sample),1);
    %y:待分类样本sample在投影方向alpha上的投影
    for i=1:size(obj.train,2)
        y=y+obj.alpha(i) * kernel_function(obj.train(:,i),sample);
    end
    %决策分类
    if y>obj.critical_y & obj.mean1>obj.mean2%如果mean1>mean2且y>临界值，则y属于class1
        classfit=1;
    elseif y<obj.critical_y & obj.mean1<obj.mean2%如果mean1<mean2且y<临界值，则y也属于class1
        classfit=1;
    else%其余情况，则y属于class2
        classfit=2;
    end
end