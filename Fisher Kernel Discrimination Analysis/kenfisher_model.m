%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%函数名称：kenfisher_model()
%参数class1 class2：两类训练样本的所有特征
%返回值：km：训练得出的投影向量
%函数功能：基于核的Fisher分类器的训练函数
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function km=kenfisher_model(class1,class2)
%  load templet pattern;
     num1=size(class1,1);%class1选取样品个数, e.g.class1 130x25 vector
     num2=size(class2,1);%class2选取样品个数, e.g.class2 130x25 vector
     m1=[];
     m2=[];
     num=num1+num2; %样品总个数
     %将两类样品合在一起存放在矩阵p中
     p=[class1',class2'];%p is 25x260的矩阵
%      求m1，第一类样本的均值向量在最优投影方向上的投影
     for i=1:num
         m1(i)=0;
         for j=1:num1
             m1(i)=m1(i)+kernel_function(p(:,i),class1(j,:)');%m1为numx1的行向量，每个元素m1(i)由第1类中所有num1个样本与p(:,i)求核函数并求和得到
         end
         m1(i)=m1(i)/num1;
     end
     m1=m1';%m1 为numx1的列向量

%      求m2，第二类样本的均值向量在最优投影方向上的投影
     for i=1:num
         m2(i)=0;
         for j=1:num2
             m2(i)=m2(i)+kernel_function(p(:,i),class2(j,:)');%m2为numx1的行向量，每个元素m2(i)由第2类中所有num2个样本与p(:,i)求核函数并求和得到
         end
         m2(i)=m2(i)/num2;
     end
     m2=m2';%m2 为numx1的列向量

     m=(m1-m2)*(m1-m2)';%求M
     k1=[];k2=[];
%      求k1，用于求下方n
     for i=1:num
         for j=1:num1
             k1(i,j)=kernel_function(p(:,i),class1(j,:)');%k1 为260x130矩阵，为第1类样本和总样本p的核函数结果矩阵，p(:,i)和class1(j,:)都是25x1的向量
         end
     end
%      求k2，用于求下方n
     for i=1:num
         for j=1:num2
             k2(i,j)=kernel_function(p(:,i),class2(j,:)');%k2 为260x130矩阵，为第2类样本和总样本p的核函数结果矩阵， p(:,i)和class2(j,:)都是25x1的向量
         end
     end
%      求n
     I11=eye(num1);%130x130单位矩阵
     I22=eye(num2);%130x130单位矩阵
     I1=ones(num1)/num1;%130x130矩阵
     I2=ones(num2)/num2;%130x130矩阵
     n=k1*(I11-I1)*k1'+k2*(I22-I2)*k2';%n是numxnum方阵，是一个中间量，用于求a
     n=n+0.002*eye(1);%使n正定
     a = inv(n)*(m1-m2);%对n求逆，并与(m1-m2)矩阵 相乘，得到a为投影方向,它是numx1列向量
     y1=[];y2=[];
%      求两类样本的线性投影
     for i=1:num1
         y1(i)=0;
         for j=1:num
             y1(i)=y1(i)+a(j)*kernel_function(p(:,j),class1(i,:)');%通过核函数，第1类样本中每一个样本在a投影方向上的投影，所组成的投影向量,num1维
         end
     end  %y1  1x130 vector
     for i=1:num2
         y2(i)=0;
         for j=1:num
             y2(i)=y2(i)+a(j)*kernel_function(p(:,j),class2(i,:)');%通过核函数，第2类样本中每一个样本在a投影方向上的投影，所组成的投影向量,num2维
         end
     end   %y2  1x130 vector
%      求各类别在特征空间的均值
     mean1=mean(y1');%求第1类样本在特征空间投影的均值
     mean2=mean(y2');%求第2类样本在特征空间投影的均值
%      求阈值y0
     y0=(num1*mean1+num2*mean2)/(num1+num2);
%      将该两组样本（class1, class2）训练得到的阈值y0,投影方向a,
%      两类样本汇总p，两类样本在特征空间投影的均值mean1,mean2传入kenfisher_model_class类中的构造方法
     km = kenfisher_model_class(p,y0,a,mean1,mean2);
end

      