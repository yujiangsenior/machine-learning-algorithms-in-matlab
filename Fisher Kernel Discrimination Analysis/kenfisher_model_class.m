%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%类名称：kenfisher_model_class()
%属性p,critical_y,alpha：alpha为投影方向，p是传入的两类样本的汇总样本，critical_y是阈值点
%方法：kenfisher_model_class()：将输入参数转为类属性

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef kenfisher_model_class
    %类属性
	properties
        train
        critical_y
        alpha
        mean1
        mean2
    end
    %类方法
    methods
		%构造方法
        function km = kenfisher_model_class(p,critical_y,alpha, M1, M2)
            if nargin > 0
                km.train = p;%p是传入的两类样本的汇总样本
                km.critical_y = critical_y;%critical_y是阈值点
                km.alpha = alpha;%alpha：alpha为投影方向
                km.mean1=M1;%M1为第一类数据的平均投影
                km.mean2=M2;%M2为第二类数据的平均投影
            end
        end
    end %end methods
end %end classdef
