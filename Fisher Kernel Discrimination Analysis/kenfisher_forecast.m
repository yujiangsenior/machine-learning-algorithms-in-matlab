%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%函数名称：kenfisher_forecast()
%参数：km：训练得出的投影向量 sample：待识别样品特征
%返回值：class_num：返回识别出的样品标签
%函数功能：基于核的Fisher分类器的预测函数1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function class_num=kenfisher_forecast(km,sample)
    num=zeros(1,length(km));%num为打分表
for i=2:length(km)
    for j=1:i-1
        result=classify(km{i,j},sample);%每一对i,j，调用一次classify，返回该sample属于该km{i,j}中的哪一类，返回1则属于i类，返回2则属于j类
        if result==1
            num(i)=num(i)+1;
        else
            num(j)=num(j)+1;
        end
    end
end
[~,class_num]=max(num);%返回num中最大值的序号，作为预测出的标签类，这些标签类都是数字1,2,3...length(km)。
