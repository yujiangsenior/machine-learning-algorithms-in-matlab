%% 手写体分类
clc
clear

data = load('Digits.mat');
testSet= data.A(1935:2880,1:1024);
testLabels = data.A(1935:2880,1025);
trainLabels = data.A(1:1934,1025);
trainSet = data.A(1:1934,1:1024);%导入数据，并提取训练数据及测试数据

aa = tabulate(trainLabels);
index = aa(:,2);%获取训练数据各类别的数量，比如[189,198,204....]
train = mat2cell(trainSet,index,[1024]);%将trainSet数据集进行切片，10类切成10个cell，获得的train是训练数据，10x1 cell

list=testSet();%测试数据集
f_t = testLabels();%测试数据集标签

km=kenfisher_train(train);%10x10 cell,sigma(1to9)=45个有值，km{i,j}为包含train(1024x387 double),critical_y(阈值点),
%alpha(387x1 double),mean1(第一类的平均值),mean2(第二类的平均值),的class instance
clear flag
%flag是预测出的标签
for i=1:size(list,1)% i=1:1300
     flag(i,1)=kenfisher_forecast(km,list(i,:))-1;
end
%输出预测标签的正确率
length(find(f_t==flag))/length(f_t)

