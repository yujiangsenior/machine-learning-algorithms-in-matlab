%% example 1-鸢尾花聚类
%spectral clustering
clear
load fisheriris;
k=3;
ID=spectral_clustering(meas,k);

label=categorical(species);
labels=grp2idx(label);
disp('spectral clustering correct ratio is: ');
disp(sum(ID==labels)/size(labels,1));

% kmeans test
clear
load fisheriris;
k=3;
ID = kmeans(meas, k);

%以下为调整获得的ID
new_ID=zeros(size(ID,1),1);
temp = ID(1);
idx = zeros(3,1);
index = 1;
idx(temp) = index;
new_ID(1)=index;
for i=2:size(ID,1)
    if ID(i)==temp;
        new_ID(i)=index;
    else
        if idx(ID(i))==0
            temp=ID(i);
            index=index+1;
            idx(temp) = index;
            new_ID(i)=index;
        else
            new_ID(i)=idx(ID(i));
        end
    end
end%调整结束
label=categorical(species);
labels=grp2idx(label);
disp('kmeans clustering correct ratio is: ');
disp(sum(new_ID==labels)/size(labels,1));

%% example 2: 内外两个圆圈
% spectral_clustering
clear
load('circledata.mat');
X=data';
plot(X(:,1),X(:,2),'b+')
k=2;
ID=spectral_clustering(X,k);

figure(1);
plot(X(ID==1,1),X(ID==1,2),'r+', X(ID==2,1),X(ID==2,2),'b*');

% kmeans
clear
load('circledata.mat');
X=data';
k=2;
ID = kmeans(X, k);
figure(2);
plot(X(ID==1,1),X(ID==1,2),'r+', X(ID==2,1),X(ID==2,2),'b*');