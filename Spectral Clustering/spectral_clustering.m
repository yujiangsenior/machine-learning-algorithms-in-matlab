function ID = spectral_clustering(x, k)
    W=neighbor_matrix(x);
    D = diag(sum(W));
    L = D-W;
    L2=inv(sqrt(D))*L*inv(sqrt(D));
    [VV dummy] = eig(L2);%求特征值和特征向量，特征值放到dummy，特征向量组成V矩阵
    [eigValues, idx] = sort(diag(dummy), 'ascend');%将特征值从小到大排列
    V = VV(:, idx(1:k));

    ID = kmeans(V, k);
%以下为调整获得的ID
    new_ID=zeros(size(ID,1),1);
    temp = ID(1);
    idx = zeros(k,1);
    index = 1;
    idx(temp) = index;
    new_ID(1)=index;
    for i=2:size(ID,1)
        if ID(i)==temp;
            new_ID(i)=index;
        else
            if idx(ID(i))==0
                temp=ID(i);
                index=index+1;
                idx(temp) = index;
                new_ID(i)=index;
            else
                new_ID(i)=idx(ID(i));
            end
        end
    end%调整结束
    ID=new_ID;
end

%%%%%%%%%%%%%%%%%%%%%%%
%求数据矩阵x（n行，每行为一个样本）的邻接矩阵w（相似矩阵），w为n*n为方阵
function w=neighbor_matrix(x)
    n = size(x,1);
    w = zeros(n,n);%初始化矩阵w，w为n*n的全部为0的方阵
    sigma=1;
    for i = 1:n
        for j = 1:i
            w(i,j) = exp(-norm((x(i,:)-x(j,:)),2)/(2*sigma*sigma));%按元素求协方差矩阵，求出的w为下三角矩阵
        end
    end
    w = w + tril(w,-1)';%将下三角矩阵的上半部分按对角线对称补齐，即最后得到的w为对称矩阵
end