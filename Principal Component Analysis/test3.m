    %% slice_localization 回归
    clear
    load 'slice';
    train=slice(1:end,1:385);
    [coeff, score, latent, tsquared, explained,mu] = pca(train,'NumComponents',385,'Algorithm','svd');
    
    isplot=1; %是否画出可视化图，1为画图，0为不画图;
    di = 1; %距离方案，1为欧拉距离，2为曼哈顿距离，3为夹角余弦;
    K = 19; %knn近邻个数;
    isclassify = 0; %isclassify=1为分类，isclassify=0为回归;
    testLabels=slice(50001:end,386);
    trainLabels=slice(1:50000,386);
    TestSet =score(50001:end,1:3);
    trainSet=score(1:50000,1:3);
    dimensions=size(trainSet,2);
    tic
    regression2 = KNN(TestSet,trainSet,trainLabels,dimensions,K, di, isplot, isclassify,testLabels);
    toc
    
    
    testLabels=slice(50001:end,386);
    trainLabels=slice(1:50000,386);
    TestSet =slice(50001:end,1:385);
    trainSet=slice(1:50000,1:385);
    dimensions=size(trainSet,2);
    tic
    regression = KNN(TestSet,trainSet,trainLabels,dimensions,K, di, isplot, isclassify,testLabels);
    toc
%% 对比KNN PCA+KNN    
    figure(1)
    hold on
    plot(1:length(regression1),regression1);
    plot(1:length(regression2),regression2);
    legend('KNN outcome','KNN outcome after PCA');
    hold off
%% redwine regression
    load Wine;
    train = A(:,1:11);
    [coeff, score, latent, tsquared, explained] = pca(train,'NumComponents',5);
    

%直接对输入的原始数据进行KNN回归
    isplot=1; %是否画出可视化图，1为画图，0为不画图;
    di = 1; %距离方案，1为欧拉距离，2为曼哈顿距离，3为夹角余弦;
    K = 15; %knn近邻个数;
    isclassify = 0; %isclassify=1为分类，isclassify=0为回归;
    testLabels = A(1001:1599,12);
    TestSet = A(1001:1599,1:11);
    trainLabels = A(1:1000,12);
    trainSet = A(1:1000,1:11);
    dimensions=size(trainSet,2);
    tic
    regression = KNN(TestSet,trainSet,trainLabels,dimensions,K, di, isplot, isclassify,testLabels);
    toc

%先进行PCA，再对score中的数据进行KNN回归
    isplot=1; %是否画出可视化图，1为画图，0为不画图;
    di = 1; %距离方案，1为欧拉距离，2为曼哈顿距离，3为夹角余弦;
    K = 15; %knn近邻个数;
    isclassify = 0; %isclassify=1为分类，isclassify=0为回归;
    testLabels = A(1001:1599,12);
    TestSet = score(1001:1599,1:5);
    trainLabels = A(1:1000,12);
    trainSet = score(1:1000,1:5);
    dimensions=size(trainSet,2);
    tic
    regression2 = KNN(TestSet,trainSet,trainLabels,dimensions,K, di, isplot, isclassify,testLabels);
    toc

%对比不同algorithm
    %'eig'
    train = A(:,1:11);
    [coeff, score, latent, tsquared, explained] = pca(train, 'Algorithm', 'eig','NumComponents',5);
    isplot=1; %是否画出可视化图，1为画图，0为不画图;
    di = 1; %距离方案，1为欧拉距离，2为曼哈顿距离，3为夹角余弦;
    K = 15; %knn近邻个数;
    isclassify = 0; %isclassify=1为分类，isclassify=0为回归;
    testLabels = A(1001:1599,12);
    TestSet = score(1001:1599,1:5);
    trainLabels = A(1:1000,12);
    trainSet = score(1:1000,1:5);
    dimensions=size(trainSet,2);
    tic
    regression1 = KNN(TestSet,trainSet,trainLabels,dimensions,K, di, isplot, isclassify,testLabels);
    toc
    
    %对比不同algorithm
    %'als'
    train = A(:,1:11);
    [coeff, score, latent, tsquared, explained] = pca(train, 'Algorithm', 'als','NumComponents',5);
    isplot=1; %是否画出可视化图，1为画图，0为不画图;
    di = 1; %距离方案，1为欧拉距离，2为曼哈顿距离，3为夹角余弦;
    K = 15; %knn近邻个数;
    isclassify = 0; %isclassify=1为分类，isclassify=0为回归;
    testLabels = A(1001:1599,12);
    TestSet = score(1001:1599,1:5);
    trainLabels = A(1:1000,12);
    trainSet = score(1:1000,1:5);
    dimensions=size(trainSet,2);
    tic
    regression3 = KNN(TestSet,trainSet,trainLabels,dimensions,K, di, isplot, isclassify,testLabels);
    toc
    
%可视化
    figure(1)
    hold on
    plot(1:length(regression1),regression1,'k');
    plot(1:length(regression2),regression2,'r');
    plot(1:length(regression3),regression3,'b');
    legend('KNN outcome after EIG','KNN outcome after SVD','KNN outcome after ALS');
    hold off
    mean(regression1==regression2)
    mean(regression3==regression2)
    
%     a=regression-testLabels;
%     b=regression2-testLabels;
%     mean(a./testLabels)
%     mean(b./testLabels)
