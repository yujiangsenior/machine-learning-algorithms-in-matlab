%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [coeff, score, latent, tsquared, explained, mu] = pca(x,varargin)
%   主要调用语法1：
%   COEFF = PCA(X) 返回的是主成分分析的转换矩阵，COEFF为P行r列的矩阵
%   X为N行P列矩阵，X的一行代表一个样本，X的一列代表一个属性字段
%   COEFF的每一列是一个主成分的坐标向量。从左到右每一列代表的主成分方差(LATENT)是递减的
%   默认情况下，PCA,会对数据X进行中心化，并使用奇异值分解(SVD)，对于非默认参数，可以通过name/value对来选择.
%   主要调用语法2：
%   [COEFF, SCORE] = PCA(X) 返回的SCORE是数据在各个主成分上的score
%   为N-r的矩阵，每一行为一个样本，每一列为一个主成分。X有下列关系：X *COEFF= SCORE.
%   [COEFF, SCORE, LATENT] = PCA(X) 返回的LATENT是各个主成分的方差，即X的协方差矩阵的特征值

%   主要调用语法3：
%   [...] = PCA(..., 'PARAM1',val1, 'PARAM2',val2, ...)该种调用能够通过指定
%   可能参数name/value对来控制计算、处理特殊类型数据，可能参数包括:
%   
%    'Algorithm' - 用于主成分分析的算法类别，有以下:
%        'svd'   - 奇异值分解算法(默认选项).
%        'eig'   - 协方差矩阵的特征值分解
%        'als'   - 交替最小二乘(ALS), 将输入信号X分解成N-by-K的左矩阵
%                  P-by-K的右矩阵；ALS算法主要用于更好地处理存在丢失值的数据
%
%     'Centered' - 中心化的选项: 
%         true   - 默认. PCA对X按列进行中心化，每个元素减去该列的均值,
%                  然后进行特征值分解或者奇异值分解
%         false  - PCA不对数据进行中心化，X有下列关系：X*COEFF= SCORE.
%
%     'Economy'  - 控制是否输出全部主成分的。有以下:
%         true   - 默认选项. PCA只返回前D个LATENT(X的协方差矩阵的特征值)
%                  COEFF和SCORE也只包含相应的几个特征向量
%                  NOTE: 对于采用ALS算法的算例，PCA总是选取这个默认选项
%         false  - PCA返回全部的LATENT(X的协方差矩阵的特征值)
%
%     'NumComponents' - 提取的主成分的个数K，一般0 < K <= P(原X的变量个数
%
%     'Rows'     - 用于处理X包含NaN情况的项，若是ALS算法，则自动忽略
%                  这个控制量，有以下：
%         'complete' - 默认选项。包含NaN值的样本在计算前被移除

%         'pairwise' - 如果指定为这，则自动选择'eig'算法.
%                      通过计算去除含NaN值的行样本后的数据矩阵x中的I列和J列之间的协方差，
%                      即为协方差矩阵的第(I,J)个元素
%         'all'      - 如果指定为这，则X不包含任何丢失的
%                      X的都被应用，如果找到NaN，则停止执行程序
%     'Weights'  - 样本权重, 长为N的向量，每个元素都是正数
%
%     'VariableWeights' - 变量权重. 有以下：
%          - 长为P的向量，每个元素都是正数
%          - 字符'variance'. 变量权重是样本方差的逆矩阵
%            如果同时'Centered'为true,则X是要经过中心化和标准化的
%            且PCA返回的是基于关系矩阵的主成分矩阵
%
%   以下参数的name/value对，只针对使用'als'算法时增加额外的选项
%
%      'Coeff0'  - COEFF的初始矩阵，P行K列，默认值是一个随机矩阵
%
%      'Score0'  - SCORE的初始矩阵，N行K列，默认值是一个随机矩阵
%
%      'Options' - 用STATSET函数创建的选项结构，PCA使用以下属性
%          'Display' - 显示输出的程度，选项off'(默认),'final','iter'.
%          'MaxIter' - 允许的最大步数，默认100，达到MaxIter就认为达到收敛条件
%           'TolFun' - 正数，给定了损失函数迭代的终止条件，默认值是1e-6.
%             'TolX' - 正数，给定了L和R中各个元素变化的收敛阈值，默认值是1e-6.
%
%   调用示例:
%       load hald;
%       [coeff, score, latent, tsquared, explained] = pca(ingredients);


[n, p] = size(x);%获取输入数据的维度，n：样本个数，p:每一样本属性字段个数

%解析输入参数，并判断格式是否符合要求
paramNames = {'Algorithm','Centered','Economy','NumComponents','Rows',...
    'Weights','VariableWeights','Coeff0','Score0','Options'};%给定可输入的控制参数的名称
defaults   = {'svd',       true,      true,    p,           'complete',...
    ones(1,n) ,ones(1,p),        [],      [], statset('pca')};%给定可输入的控制参数的默认值

[vAlgorithm, vCentered, vEconomy, vNumComponents, vRows,vWeights,...
    vVariableWeights, c0, s0, opts, setFlag]...
    = internal.stats.parseArgs(paramNames, defaults, varargin{:});%根据输入参数值的情况，结合defaults里面的默认值，对等式左边各变量初始化
%'Algorithm'参数值处理
AlgorithmNames = {'svd','eig','als'};%判断输入参数'Algorithm'参数的值vAlgorithm是否合法
vAlgorithm = internal.stats.getParamVal(vAlgorithm,AlgorithmNames,...
    '''Algorithm''');
%'Centered'参数值处理
vCentered = internal.stats.parseOnOff(vCentered,'''Centered''');%判断输入参数'Centered'参数的值vCentered是否合法
%'Economy'参数值处理
vEconomy = internal.stats.parseOnOff(vEconomy,'''Economy''');%判断输入参数'Economy'参数的值vEconomy是否合法
%'NumComponents'参数值处理
if ~isempty(x) && ~internal.stats.isScalarInt(vNumComponents,1,p)
    error(message('stats:pca:WrongNumComponents',p));%判断输入参数'NumComponents'参数的值vNumComponents是否合法
end
%'Rows'参数值处理
RowsNames = {'complete','pairwise','all'};
vRows = internal.stats.getParamVal(vRows,RowsNames,'''Rows''');%判断输入参数'Rows'参数的值vRows是否合法

switch vAlgorithm
    case 'svd'
        %如果'Rows'参数值为'pairwise'，则转到'eig'算法
        if strcmp(vRows,'pairwise')
            if setFlag.Algorithm
                warning(message('stats:pca:NoPairwiseSVD'));
            end
            vAlgorithm = 'eig';
        end
		%如果调用时指定了'Coeff0''Score0'参数值，则转到'als'算法
        if setFlag.Coeff0 || setFlag.Score0
            vAlgorithm = 'als';
        end
    case 'als'
        %如果调用'als'，则自动忽略'Rows'选项
        if setFlag.Rows
            warning(message('stats:pca:NoALSRows'));
        end
end

%'Weights'参数值处理
if isvector(vWeights) && isequal(numel(vWeights),n)%判断输入参数'Weights'参数的值vWeights是否合法
    vWeights = reshape(vWeights,1,n); % 确保vWeights是一个行向量
else
    error(message('stats:pca:WrongObsWeights', n));
end

if internal.stats.isString(vVariableWeights)%输入参数'VariableWeights'参数的值vVariableWeights是否合法
    WeightsNames = {'variance'};
    internal.stats.getParamVal(vVariableWeights,WeightsNames,...
        '''VariableWeights''');
    vVariableWeights = 1./classreg.learning.internal.wnanvar(x,vWeights,1);
	%忽略x中NaN的加权方差：以vWeights为权重，对X按行加权计算方差得到1xP的行向量，再对每个元素求倒数，赋给vVariableWeights
elseif isnumeric(vVariableWeights) && isvector(vVariableWeights)...
        && (isequal(numel(vVariableWeights), p))%如果vVariableWeights是含p个数值元素的向量
    vVariableWeights = reshape(vVariableWeights,1,p);% 确保vVariableWeights是一个行向量
else
    error(message('stats:pca:WrongVarWeights', p));
end

if any(vWeights <= 0) || any(vVariableWeights <= 0)
    error(message('stats:pca:NonPositiveWeights'));
end
% 结束对输入参数的处理


% 处理输入数据x是空集的情况
if isempty(x)
    pOrZero = ~vEconomy * p;
    coeff = zeros(p, pOrZero); 
    coeff(1:p+1:end) = 1;
    score = zeros(n, pOrZero);
    latent = zeros(pOrZero, 1);
    tsquared = zeros(n, 1);
    explained = [];
    mu = [];
    return;
end

nanIdx = isnan(x);%返回的nanIdx是判断x中各个元素是否为NAN的布尔值矩阵
numNaN = sum(nanIdx, 2); %每一行NaNs出现个数
wasNaN = any(numNaN,2); %包含NaN的行的行数

% 处理输入数据x全是NaNs的情况，所有变量返回NaN
if all(nanIdx(:))
    coeff = NaN;
    score = NaN;
    latent = NaN;
    tsquared = NaN;
    explained = NaN;
    mu = NaN;
    return;
end
%处理特殊标量的情况
if isscalar(x)
    coeff = 1;
    score = (~vCentered)*x;%vCentered默认为1，~vCentered默认为0，即返回score为0向量
    latent = (~vCentered)*x^2;
    tsquared = ~vCentered;
    explained = 100;
    mu = vCentered*x;
    return;
end

if strcmp(vRows,'all') && (~strcmp(vAlgorithm,'als'))%如果Rows参数为'all'，同时Algorithm参数不是'als'，则执行下述判断
    if any(wasNaN)%如果wasNaN中有非零项，则报错
        error(message('stats:pca:RowsAll'));
    else%如果wasNaN中没有非零项，则将Rows参数转为'complete'
        vRows = 'complete';
    end
end

if strcmp(vRows,'complete')%如果Rows参数为'complete'
    %如果中心化，则自由度(DOF)为n-1，如果不是，则自由度为n，其中n是不含NaN元素的行数
    DOF = max(0,n-vCentered-sum(wasNaN));
elseif strcmp(vRows,'pairwise')%如果Rows参数为'pairwise'
    % 自由度DOF，取不含NaNs的元素对的最大对数
    notNaN = double(~nanIdx);%notNaN即为将nanIdx中所有元素取逻辑反的布尔值矩阵
    nanC = notNaN'*notNaN;
    nanC = nanC.*(~eye(p));
    DOF = max(nanC(:));
    DOF = DOF-vCentered;
else%如果Rows参数为'all'
    DOF = max(0,n-vCentered);%如果中心化，则自由度(DOF)为n-1，如果不是，则自由度为n
end

%%计算以vWeights为权重的加权样本均值:
mu = classreg.learning.internal.wnanmean(x, vWeights);

%如果没有给定任何权重参数，直接用EIG算法进行计算
switch vAlgorithm
case 'eig'
    %如果'Centered'参数值为true，则对X进行中心化
    if vCentered
        x = bsxfun(@minus,x,mu);%对样本进行中心化，即样本-样本均值
    end
    
    %调用localEIG()函数计算主成分
    [coeff, eigValues] = localEIG(x, vCentered, vRows, vWeights,...
        vVariableWeights);
    
    %如果'Economy'参数值为true，不用返回任何对应于特征值为0的特征向量
    if (DOF<p)%p是输入数据x的属性字段个数，如果自由度<属性字段个数，则执行下述
        if vEconomy%如果'Economy'参数值为true，则coeff后面那些列都为空，eigValues后面那些值都为空
            coeff(:, DOF+1:p) = [];
            eigValues(DOF+1:p, :) = [];
        else%如果'Economy'参数值为false，则需要返回0特征值对应的特征向量，即保留coeff后面那些列
            eigValues(DOF+1:p, :) = 0;
        end
    end
    
    %判断特征值是否均为正
    if any(eigValues<0)
        error(message('stats:pca:CovNotPositiveSemiDefinite'));
    end
    
    if nargout > 1%如果调用语句的输出变量多于1个
        score = x/coeff';%输出score矩阵,即score=x*coeff
        latent = eigValues;%输出特征值latent
        if nargout > 3%如果调用语句的输出变量多于3个
            tsquared = localTSquared(score, latent, n, p);
			%调用localTSquared()来输出tsquared，表示各个样本点离样本中心的距离
        end
    end
    
case 'svd' %使用SVD算法进行PCA计算
    %如果'Centered'参数值为true，则对X进行中心化
    if vCentered
        x = bsxfun(@minus,x,mu);%对样本进行中心化，即样本-样本均值
    end
    
    [U,sigma, coeff, wasNaN] = localSVD(x, n,...
        vEconomy, vWeights, vVariableWeights);%调用localSVD()函数计算主成分
    if nargout > 1%针对输出变量个数，进行输出情况处理
        score =  bsxfun(@times,U,sigma');%sigma为p*1的列向量，则sigma'为1*p的行向量
		%计算输出变量score，score=U*sigma'，此*为用sigma'中的每个元素乘U中对应列
        latent = sigma.^2./DOF;%输出特征值latent
        if nargout > 3
            tsquared = localTSquared(score,latent,DOF,p);
			%调用localTSquared()来输出tsquared，表示各个样本点离样本中心的距离
        end
        %将NaNs对应的元素插回到score,tsquared两个变量
        if any(wasNaN)
            score = internal.stats.insertnan(wasNaN, score);
            if nargout >3
                tsquared = internal.stats.insertnan(wasNaN,tsquared);
            end
        end
    end
    
    if DOF < p%p是输入数据x的属性字段个数，如果自由度<属性字段个数，则执行下述
        if vEconomy%如果'Economy'参数值为true，不用返回任何对应于特征值为0的特征向量
            coeff(:, DOF+1:end) = [];
            if nargout > 1
                score(:, DOF+1:end)=[];
                latent(DOF+1:end, :)=[];
            end
        elseif nargout > 1%如果'Economy'参数值为false，则需要返回0特征值对应的特征向量，即保留coeff后面那些列
        % 否则，特征值和对应的输出坐标score都得设为0
            score(:, DOF+1:p) = 0;
            latent(DOF+1:p, 1) = 0;
        end
    end
    
case 'als' %交替最小二乘算法Alternating Least Square
    
    vNumComponents = min([vNumComponents,n-vCentered,p]);  % ALS总是返回economy sized的输出，即ALS输出的主成分个数总是比较小，这能减少工作量
    
    if isempty(s0);%s0是SCORE的初始矩阵，如果s0为空，则随机生成矩阵给s0
        s0 = randn(n,vNumComponents);
    elseif ~isequal(size(s0),[n,vNumComponents])|| any(isnan(s0(:)))
        error(message('stats:pca:BadInitialValues','Score0',n,vNumComponents));
    end
    if isempty(c0);%c0是COEFF的初始矩阵，如果初始为空（默认），则随机生成p行vNumComponents列的矩阵赋给c0
        c0 = randn(p,vNumComponents);
    elseif ~isequal(size(c0),[p,vNumComponents]) || any(isnan(c0(:)))
        error(message('stats:pca:BadInitialValues','Coeff0',p,vNumComponents));
    end
     
    [score,coeff,mu,latent]=alsmf(x,vNumComponents,'L0',s0,'R0',c0,...
        'Weights',vWeights,'VariableWeights',vVariableWeights,...
        'Orthonormal',true,'Centered',vCentered,'Options',opts);
		%调用alsmf()函数计算主成分
    
    if nargout > 3
        % T-squared值对应的是压缩后的数据坐标
        tsquared = localTSquared(score, latent,n-vCentered, vNumComponents);%输出tsquared
    end
end %选择算法部分结束

% 计算每个主成分下方差占总方差的百分比
if nargout > 4
    explained = 100*latent/sum(latent);%latent是一个向量
end

% 控制输出，仅仅输出前K个主成分
if (vNumComponents<DOF)
    coeff(:, vNumComponents+1:end) = [];
    if nargout > 1
    score(:, vNumComponents+1:end) = [];
    end
end


% 在系数上执行符号约定，每列中最大的元素将有正标签
[~,maxind] = max(abs(coeff), [], 1);%输出的maxind是一个1*p的行向量，每个元素对应coeff中相应列中，绝对值最大的元素所在的行标签
[d1, d2] = size(coeff);%d1为coeff的行数，d2为列数
colsign = sign(coeff(maxind + (0:d1:(d2-1)*d1)));
%(0:d1:(d2-1)*d1)返回的是1*p的行向量，加上maxind（1*p），即得到各列中绝对值最大元素在coeff中的序号。colsign中1代表正数，-1代表负数
coeff = bsxfun(@times, coeff, colsign);%对coeff每一列乘以该列的绝对值最大数的符号（正或负，即1或者-1）
if nargout > 1
    score = bsxfun(@times, score, colsign); %对score每一列乘以该列的绝对值最大数的符号（正或负，即1或者-1）
end

end %结束主函数
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------Subfucntions--------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%该函数用于应用EIG算法计算主成分
function [coeff, eigValues]=localEIG(x,vCentered, vRows,vWeights,...
        vVariableWeights)
% 用EIG算法进行计算，vRows是计算协方差矩阵时处理NaN的参数，vWeights和vVariableWeights是样本权重和变量权重.

% 应用样本权重和变量权重对x进行调整
OmegaSqrt = sqrt(vWeights);%对vWeights求平方根
PhiSqrt = sqrt(vVariableWeights);%对vVariableWeights求平方根
x = bsxfun(@times, x, OmegaSqrt');%矩阵x和矩阵OmegaSqrt的转置相乘
x = bsxfun(@times, x, PhiSqrt);%矩阵x和矩阵PhiSqrt相乘

xCov = ncnancov(x, vRows, vCentered);%求x的协方差矩阵

[coeff, eigValueDiag] = eig(xCov);%求特征值和特征向量，特征值放到eigValueDiag，特征向量组成coeff矩阵
[eigValues, idx] = sort(diag(eigValueDiag), 'descend');%将特征值从大到小排列
coeff = coeff(:, idx);%获取特征向量组成的转换矩阵，并按特征值从大到小排序

coeff = bsxfun(@times, coeff,1./PhiSqrt');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%该函数用于应用SVD算法计算主成分
function [U,sigma, coeff, wasNaN] = localSVD(x, n,...,
    vEconomy, vWeights, vVariableWeights)
%用SVD算法进行计算，vWeights和vVariableWeights是样本权重和变量权重.

%移除NaN的那些数据，并记录相应位置
%Remove NaNs missing data and record location
[~,wasNaN,x] = internal.stats.removenan(x);
if n==1  %处理特殊情况，因为internal.stats.removenan将所有向量视为列向量
    wasNaN = wasNaN';
    x = x';
end

%应用样本权重和变量权重对x进行调整
vWeights(wasNaN) = [];
OmegaSqrt = sqrt(vWeights);
PhiSqrt = sqrt(vVariableWeights);
x = bsxfun(@times, x, OmegaSqrt');
x = bsxfun(@times, x, PhiSqrt);
    
if vEconomy%如果'Economy'参数为true
    [U,sigma,coeff] = svd(x,'econ');%求奇异值，U为左奇异矩阵，coeff为右奇异矩阵，sigma为奇异值矩阵
else%如果'Economy'参数为false
    [U,sigma, coeff] = svd(x, 0);
end

U = bsxfun(@times, U, 1./OmegaSqrt');%应用样本权重对U进行调整
coeff = bsxfun(@times, coeff,1./PhiSqrt');%应用变量权重对coeff进行调整
%控制输出：sigma变量
if n == 1% sigma可能只有1个元素
    sigma = sigma(1);
else
    sigma = diag(sigma);%取sigma矩阵的对角线元素
end

end%localSVD函数结束
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function tsquared = localTSquared(score, latent,DOF, p)
% 用于计算霍特林T平方分布的子函数. 它是标准化后的scores平方和, 例如Mahalanobis distances.
% When X appears to have column rank < r, ignore components that are orthogonal to the data.

if isempty(score)%如果score为空，则返回空的tsquared
    tsquared = score;
    return;
end

r = min(DOF,p); %x可能的最大秩;
if DOF > 1
    q = sum(latent > max(DOF,p)*eps(latent(1)));%大于0的特征值个数
    if q < r
        warning(message('stats:pca:ColRankDefX', q));
    end
else
    q = 0;
end
standScores = bsxfun(@times, score(:,1:q), 1./sqrt(latent(1:q,:))');%对score进行标准化，赋给standScores
tsquared = sum(standScores.^2, 2);%对standScores按行求该行各元素的平方和，因此是nx1列向量
end%结束localTSquared()函数
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%求x的协方差矩阵
function c = ncnancov(x,Rows,centered)%返回X'*X/N,N为去除丢失值后的样本个数
%   C = NCNANCOV(X)返回的是X'*X/N，其中N为去除丢失值后的样本个数
%
%   C = NCNANCOV(...,'pairwise')其它计算同上，但是在计算C(I,J)时，
%   不计算在I,J列的属性字段上带NaN值的样本
%   C = NCNANCOV(...,'complete')是默认算法，它省略了任何含NaN(任何字段)的样本
%
%   C = NCNANCOV(...,true), C如果已经中心化了，那么C用N-1进行标准化；默认为非中心化

if nargin <2%如果输入变量个数为1或0，则包含NaN值的样本在计算前被移除
    Rows = 'complete';
end

d = 0;
if nargin>2
    d =  d + centered;%d用于计算协方差矩阵c时产生1/(n-d)，将centered值false或true转为0或1
end

idxnan = isnan(x);%返回的idxnan是判断x中各个元素是否为NAN的布尔值矩阵

[n, p] = size(x);%获取输入数据的维度，n:样本个数，p:每一样本属性字段个数

if ~any(any(idxnan))
%any(idxnan)返回一个1*p的行向量，都是布尔值;any(any(idxnan))再对返回的布尔值行向量进行汇总，即：如果x没有NaN，则直接计算协方差矩阵
    c = x'*x/(n-d);
elseif strcmp(Rows,'complete')%如果'Rows'参数为'complete'，'complete'即：包含NaN值的样本在计算前被移除
    nanrows = any(idxnan,2);%返回的nanrows为n行1列的布尔值列向量，目的是取得那些带NaN的样本的行号
    xNotNaN = x((~nanrows),:);%返回的xNotNaN为x去掉那些含NaN值的样本后的样本集
    denom = max(0, (size(xNotNaN,1)-d));%取denom=n-d,n为xNotNaN的行数
    c = xNotNaN'*xNotNaN/denom;%计算协方差矩阵
elseif strcmp(Rows,'pairwise')
%如果'Rows'参数为'pairwise'，'pairwise'即：通过计算去除含NaN值的行样本后的数据矩阵x中的I列和J列之间的协方差，即为协方差矩阵的第(I,J)个元素
    c = zeros(p,class(x));%初始化矩阵c，c为p*p的全部为0的方阵
    for i = 1:p
        for j = 1:i
            NonNaNRows = ~any(idxnan(:,[i, j]),2);%在x的第i,j列是否有NaN值出现，排除那些样本后取剩余行号，返回的NonNaNRows是n行1列的布尔值矩阵
            denom = max(0,(sum(NonNaNRows)-d));%取denom=n-d,n为NonNaNRows的行数
            c(i,j) = x(NonNaNRows,i)'*x(NonNaNRows,j)/denom;%按元素求协方差矩阵，求出的c为下三角矩阵
        end
    end
    c = c + tril(c,-1)';%将下三角矩阵的上半部分按对角线对称补齐，即最后得到的c为对称矩阵
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%