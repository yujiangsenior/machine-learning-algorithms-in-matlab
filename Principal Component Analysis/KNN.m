% Project Title: KNN
% Group: 南华期研所
% Developer: 蒋宇
% date:2018/04/23
% Contact Info: jyu2007abc@sina.com
% Input: 
% inx: double vector n x d, d为输入数据维度
% data: double vector N x D ,D为样本数据维度
% labels: double or cell(char in each cell) vector N x 1, N为样本数据数量
% dimensions: double, 标签种类数量
% k: double, KNN近邻个数
% di: double, 选择距离计算公式；di = 1为欧拉距离，di = 2为曼哈顿距离，di = 3为夹角余弦
% isplot: double, 是否运行可视化结果部分的代码，isplot = 1为运行，isplot = 0为不运行
% isClassify: double, isClassify=1为分类问题，isClassify=0为回归问题
% 为控制KNN进行分类或者回归的开关，isClassify=1运行分类算法，isClassify=0运行回归算法
% test_labels: vector n x 1, n为测试数据数量（该数据可输入也可不输入）
% Output: KNN分类/回归结果

% e.g:
% clear
% clc
% isplot=1; %是否画出可视化图，1为画图，0为不画图;
% di = 1; %距离方案，1为欧拉距离，2为曼哈顿距离，3为夹角余弦;
% isclassify = 0; %isclassify=1为分类，isclassify=0为回归;
% K = 15; %knn近邻个数;
% if isclassify == 0    %回归;
%     data = load ('Wine.mat');
%     testLabels = data.A(1001:1599,12);
%     TestSet = data.A(1001:1599,1:11);
%     trainLabels = data.A(1:1000,12);
%     trainSet = data.A(1:1000,1:11);
%     dimensions=size(trainSet,2);
% else %分类
%     %%
%     data = load ('Digits.mat');
%     testLabels = data.A(1935:2880,1025);
%     TestSet = data.A(1935:2880,1:1024);
%     trainLabels = data.A(1:1934,1025);
%     trainSet = data.A(1:1934,1:1024);
%     dimensions=size(trainSet,2);
% end
% KNN(TestSet,trainSet,trainLabels,dimensions,K, di, isplot, isclassify,testLabels);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function resultLabel = KNN(inx,data,labels,dimensions,k,di,isplot,isClassify,varargin)
    if (~strcmp(class(inx),'double'))
        fprintf('input error! test data should be double!');
    end
    if (~strcmp(class(data),'double'))
        fprintf('input error! train data should be double!');
    end
    [inx_row, inx_col] = size(inx);%  inx为输入测试数据, n*d_feature，n行被测数据
    [data_row, data_col] = size(data);%  data为样本数据，N*D_feature，N行训练数据
    if inx_col ~= dimensions
        if inx_row~=dimensions
             fprintf('input error!');
             return;
        else
            inx=inx';
        end
    end %调整inx
    if data_col ~= dimensions
        if data_row~=dimensions
             fprintf('input error!');
             return;
        else
            data=data';
        end
    end %调整data
    [inx_row, inx_col] = size(inx);% inx为输入测试数据, n*d_feature，n行被测数据
    [data_row, data_col] = size(data);%  data为样本数据，N*D_feature，N行训练数据
    [labels_row, labels_col] = size(labels);%  labels为样本标签,N*1，一列标签
    
    if labels_row ~= data_row
        if labels_col ~= data_row
             fprintf('input error!');
             return;
        else
             labels=labels';
        end
    end %调整labels
    [labels_row, labels_col] = size(labels);%  labels为样本标签,N*1，一列标签
    
    if (strcmp(class(labels),'cell'))
        c=categorical(labels);
        labels_1=grp2idx(c);
    elseif(strcmp(class(labels),'double'))
        labels_1 = labels;%labels_1为训练数据的标签数组
    else
         fprintf('input error!');
         return;
    end
    
    if nargin==9
        test_labels = varargin{1};
        [testlabels_row, testlabels_col] = size(test_labels);%  tesetlabels为测试标签,n*1，一列标签
        if testlabels_row ~= inx_row
            if testlabels_col ~= inx_row
                 fprintf('input error!');
                 return;
            else
                 test_labels=test_labels';
            end
        end %调整test_labels        
       [testlabels_row, testlabels_col] = size(test_labels);%  test_labels为测试标签,n*1，一列标签
        if (strcmp(class(test_labels),'cell'))
            c=categorical(test_labels);
            test_labels_1=grp2idx(c);
        elseif(strcmp(class(test_labels),'double'))
            test_labels_1 = test_labels;%test_labels_1为训练数据的标签数组
        else
             fprintf('input error!');
             return;
        end
    end
    
    %% 分类或回归部分
    
    distanceMat=zeros(data_row,inx_row);%距离数组
    if di == 1
            distanceMat = pdist2(data,inx,'euclidean');% inx: n x d, data: N x D,distanceMat:N x n
    elseif di == 2
            distanceMat = pdist2(data,inx,'cityblock');
    elseif di == 3
            distanceMat = pdist2(data,inx,'cosine');
    end
    [B , SNarr] = sort(distanceMat,'ascend');%对上述所得n*1矩阵进行升序排序，所得B为升序后数组，SNarr为序列号数组；
    len = min(k,length(B));
    C = repmat(labels_1,1,inx_row);
    CC = C(SNarr(1:len,:));
    aa= mat2cell(CC,[len],ones(inx_row,1));%将CC这个len行，inx_row列的矩阵划分为1行inx_row列的cell矩阵；
    
    if isClassify == 1 %isClassify == 1,分类情况
        averages = cellfun(@tabulate,aa,'UniformOutput', false);%averages为对aa中的inx_row个cell元素进行tabulate；
        for i=1:inx_row
            U=averages{i};%averages{i}就是一个lens行3列的double数组
            [maxr(i),index(i)]=max(U(:,3));%根据U的第三列百分比数据找出k近邻中出现最多的标签所在行标，放入index；
            lens=size(U,1);%lens是average{i}的行数
            resultLabel(i)=U(index(i),1);%得到该行数据的预测标签，存入resultLabel
        end
		if nargin==9
			disp(sum(test_labels_1==resultLabel')/size(test_labels_1,1));
        end
    else%isClassify == 0,回归情况
        for i=1:inx_row
            CW=aa{i};
            resultLabel(i) = sum(CW)/length(CW);%对aa{i}进行求和，并且取平均值
        end
		if nargin==9
			aa=resultLabel'-mean(test_labels_1)*ones(size(test_labels_1,1),1);
			c=test_labels_1-mean(test_labels_1)*ones(size(test_labels_1,1),1);
			r_square=1-sum(aa.*aa)/sum(c.*c);
			disp(r_square);
		end
    end
    %% 可视化数据准备
    
    if nargin==9
        if isClassify == 1
            labels_2=tabulate(labels_1);
            labels_final=labels_2(:,1);%标签数组，非重复
            N=length(labels_final);%计算标签种类
            time_s=zeros(N,1);%每一个标签类的错误预测的测试文件数
            time_s2=zeros(N,1);%每一个标签类的正确预测的测试文件数
            ee = zeros(N,2,N);%正确预测的测试文件所预测的K近邻中的标签分布记录
            %第2个N代表测试文件本身的测试标签，(N,2)是N行2列的二维矩阵，第一列存储标签，第二列存储k近邻的标签分布
            ff = zeros(N,2,N);%错误预测的测试文件所预测的K近邻中的标签分布记录
            %第2个N代表测试文件本身的测试标签，(N,2)是N行2列的二维矩阵，第一列存储标签，第二列存储k近邻的标签分布
            for s=1:N %初始化ff 
                ff(:,1,s)=labels_final;
            end
            for t=1:N %初始化ee
                ee(:,1,t)=labels_final;
            end
            error=0;
            for i=1:inx_row
                U=averages{i};
                kk = find(labels_final==test_labels_1(i));
                %根据被测试的实际标签，查找其位于标签列表的位置，比如kk=1代表被测试的标签位于标签数组第一位
                lens=size(U,1);%lens是[0,1;1,5;3,1;4,2;8,7]的行数5        

                if(resultLabel(i)~=test_labels_1(i))
                    error = error+1;
                    time_s(kk)=time_s(kk)+1;
                    for n = 1:lens
                        ff( find(labels_final==U(n,1)),2,kk)=ff(find(labels_final==U(n,1)),2,kk)+U(n,2);
                        %根据该测试文件的测试标签所在的标签列表的位置（kk），相应操作第三维度的那个切片的二维ff矩阵，将U(n,2)存入该切片的二维ff矩阵find(labels_final==U(n,1))行，第2列
                    end
                else
                    time_s2(kk)=time_s2(kk)+1;
                    for j = 1:lens
                        ee(find(labels_final==U(j,1)),2,kk)=ee(find(labels_final==U(j,1)),2,kk)+U(j,2);%同上
                    end
                end
            end
        end
    end
    
    %% 画图部分
    if nargin == 9
        if isplot
            if isClassify == 1
                for i=1:1:2*N
                    if mod(i,2)~=0
                            t=(i+1)/2;
                            subplot(N,2,i);
                            if time_s2(t)~=0
                                plot(ee(:,1,t),ee(:,2,t)/time_s2(t),'g');
                            end
                            set(gca,'XLim',[ee(1,1,t),ee(N,1,t)]);
                            set(gca,'YLim',[0,k]);
                            xlabel('Right prediction');
                            ylabel(labels_final(t));
                    else
                            t=i/2;
                            subplot(N,2,i);
                            if time_s(t)~=0
                                plot(ff(:,1,t),ff(:,2,t)/time_s(t),'r');
                            end
                            set(gca,'XLim',[ff(1,1,t),ff(N,1,t)]);
                            set(gca,'YLim',[0,k]);
                            xlabel('Wrong prediction');
                    end
                end
                suptitle('预测标签结果的平均分布（左边一列为正确预测，右边一列为错误预测）');
                fprintf('识别准确率为：%f\n',1-error/inx_row);
            else
                subplot(3,1,1);
                resultLabel=resultLabel';
                a = (resultLabel-test_labels_1).^2;
                hold on;
                hist(a);
                xlabel('squared error');
                ylabel('频数');
                hold off;
                subplot(3,1,2);
                hold on;
                plot(resultLabel);
                plot(test_labels_1);
                legend('回归值','实际值');
                xlabel('测试案例Serial No.');
                ylabel('标签/属性值');
                hold off;
                subplot(3,1,3);
                plot((resultLabel-test_labels_1));
                xlabel('测试案例Serial No.');
                ylabel('error');
                suptitle('KNN回归结果分析图');
            end
        end
    end %plot部分结束

end