function [L, R, mu, D, final] = alsmf(Y, k, varargin)
%Alternating Least Squares (ALS) low-rank matrix factorization.
%   [L,R] = alsmf(Y,K)以最近似地程度将一个N-P的矩阵Y（默认为经过中心化处理后的矩阵）分解成两个秩为K的矩阵，
%   分别为N-K的左因子矩阵和P-K的右因子矩阵R。因式分解的过程采用了迭代方法，随机给定L和R的初始值。
%
%   [L,R,MU] = alsmf(Y,K)还返回MU，是Y中样本的均值。
%   [L,R,MU,D] = alsmf(Y,K)还返回D，D是Y的协方差矩阵的前K个最大的特征值
%   [L,R,MU,D,S] = alsmf(Y,K) 还存储了收敛时的最终结果，是一个结构体，其中：
%  S.rmsResid   - Root mean squared residuals.均方根残差
%  S.NumIter    - Number of iterations.迭代次数

%   [...] = alsmf(Y,K,L0,R0,'PARAM1',val1,'PARAM2',val2,...) 该种调用能够通过指定
%   可选参数name/value对来定制算法，可选参数包括:
%
%       'L0'         - L的初始矩阵，N行K列，默认值是一个随机矩阵
%
%       'R0'         - R的初始矩阵，P行K列，默认值是一个随机矩阵
%
%       'Orthonormal'- 表明返回的R的每一行是否正交(in classic PCA basis).
%
%         'Weights'  - 样本权重，长为N的向量，所有元素为正数
%
%  'VariableWeights' - 变量权重，一个长为P的向量，每个元素都是正数
%
%          'Options' - 一个用STATSET函数创建的选项结构，ALSMF使用以下属性:
%             'Display' - 显示输出的程度。选项有'off'(默认),'final',和'iter'.
%             'MaxIter' - 允许的最大步数，默认是100，达到MaxIter就认为达到收敛条件
%              'TolFun' - 正数，给定了损失函数迭代的终止条件，默认值是1e-6.
%                'TolX' - 正数，给定了L和R中各个元素变化的收敛阈值，默认值是1e-6.
%
%
%   调用示例:
%      >> load fisheriris;
%      >> [score,coeff,mu] = alsmf(meas,4,'Orth',true);
%      >> [score,coeff,mu] = alsmf(meas,3,'L0',rand(size(meas,1),3));
%


[n, p ]= size(Y); %获取输入数据的维度，n：样本个数，p:每一样本属性字段个数，没有针对Y和k的错误检查

inpar = inputParser;
inpar.addParamValue('L0',randn(n,k),@(x)isnumeric(x)&&isequal(size(x),[n,k]));
inpar.addParamValue('R0',randn(p,k),@(x)isnumeric(x)&&isequal(size(x),[p,k]));

inpar.addParamValue('Orthonormal',true,@islogical);
inpar.addParamValue('Centered',true,@islogical);

chkweightsfunc = @(x,siz)isnumeric(x)&&length(x)==siz&&all(x>=0);
inpar.addParamValue('Weights',ones(n,1),@(x)chkweightsfunc(x,n));
inpar.addParamValue('VariableWeights',ones(1,p),@(x)chkweightsfunc(x,p));

dftOpt = statset('alsmf');
inpar.addParamValue('Options',dftOpt);
%以上是初始化部分，根据调用函数时输入的参数以及默认值来进行初始化

inpar.parse(varargin{:});
%根据输入参数中Options的值进行相应赋值
MaxIter = statget(inpar.Results.Options,'MaxIter',dftOpt,'fast');
TolX = statget(inpar.Results.Options,'TolX',dftOpt,'fast');
TolFun = statget(inpar.Results.Options,'TolFun',dftOpt,'fast');
displayOn = statget(inpar.Results.Options,'Display',dftOpt,'fast');

flagOrthogonal = inpar.Results.Orthonormal;%将传入的Orthonormal赋给flagOrthogonal
flagCentered = inpar.Results.Centered;%将传入的Centered赋给flagCentered
Omega = inpar.Results.Weights;%将传入的Weights赋给Omega
Phi = inpar.Results.VariableWeights;%将传入的VariableWeights赋给Phi

dispnum = strcmp(displayOn,{'off','iter','final'});

Omega = Omega(:);% 将其转成列向量,n*1
Phi = Phi(:);% 将其转成列向量,p*1

% 记录Y中缺失值的位置坐标
idxMiss = isnan(Y);

% 初始化
R = inpar.Results.R0;%将传入的R0赋给R
L = inpar.Results.L0;%将传入的L0赋给L
mu = zeros(1,p);%初始化mu，1xp向量

final.NumIter =0;%初始化final结构体中的NumIter（循环次数）
dnormOld = inf;

header = 'Iteration\t   |Delta X|\t   rms resid\n';
fmtstr = '   %3d   \t%12g\t%12g\n';
cvgCond = '';
if dispnum(2)
    fprintf(header);
end

for iter = 1: MaxIter
    % Update parameters    
    munew = localMu(L,R);%更新Y中所有样本的平均值
    Rnew = localR(L,munew);%固定L，更新当前R并存入Rnew
    Lnew = localL(Rnew,munew);%固定Rnew，更新当前L并存入Lnew
    
    % 判断是否收敛
    diff = bsxfun(@minus,Y-Lnew*Rnew',munew);
    diff(idxMiss) = 0;
    dnorm = norm(diff,'fro') / (sqrt(n*p-sum(idxMiss(:))));
    dL = max(max(abs(L-Lnew) / (sqrt(eps)+max(max(abs(Lnew))))));
    dR = max(max(abs(R-Rnew) / (sqrt(eps)+max(max(abs(Rnew))))));
    dmu = max(abs(mu-munew)) / (sqrt(eps)+max(abs(munew)));
    delta = max([dL,dR,dmu]);
    
    if (dnormOld - dnorm) < TolFun*dnorm
        cvgCond = getString(message('stats:pca:TolFunReached'));%cvgCond为收敛情况
        break;
    elseif delta < TolX
        cvgCond = getString(message('stats:pca:TolXReached'));
        break;
    elseif iter == MaxIter;
        warning(message('stats:pca:MaxIterReached', MaxIter));
        break;
    else
        % 如果继续迭代，则记录上一次迭代的结果
        dnormOld = dnorm;
        L = Lnew;
        R = Rnew;
        mu = munew;
        if dispnum(2)
        fprintf(fmtstr,iter,delta,dnorm);
        end
    end
end

D = zeros(k,1);
if flagOrthogonal%flagOrthogonal:表明返回的R的每一行是否正交，如果正交，则执行代码
    if flagCentered%flagCentered:中心化的选项: 
%         true   - 默认. ALSMF对L按列进行中心化，每个元素减去该列的均值，
%                  然后进行特征值分解或者奇异值分解。
%         false  - ALSMF不对数据进行中心化
        muL = classreg.learning.internal.wnanmean(L,Omega);%对L矩阵的行空间以Omega为权重向量进行加权;Omega:样本权重
        L = bsxfun(@minus,L,muL);%L减去均值muL，即对L进行中心化
        mu = mu+muL*R';%调整mu
    end
    
    [U, Dr] = eig(bsxfun(@times,R,Phi)'*R);%求右因子矩阵R'*R(k*k矩阵)的特征值和特征向量，U是特征向量矩阵，Dr是特征值
    Dr = diag(Dr);%取Dr矩阵的对角线元素，即Dr存储了特征值组成的列向量
    Adj = bsxfun(@times,L*U,sqrt(Dr)');%Adj为L*U*[sigma]
    [V,D] = eig(bsxfun(@times,Adj,Omega)'*Adj);%[sigma]U'*L'*L*U*[sigma]
    
    R = bsxfun(@times,R*U,1./sqrt(Dr)')*V;%调整R，R=R*U*[1/sigma]*V
    L = Adj*V;%调整L，L=L*U*[sigma]*V
    D = diag(D)/(size(L,1)-flagCentered);%D就是分解后的新坐标L的协方差矩阵L'L的特征值
    
    [D,idxD] = sort(D,'descend');%将L,R,D按照特征值D从大到小排列
    L = L(:,idxD);
    R = R(:,idxD);
end

% 显示最终的输出
if ~dispnum(1)
    finalstr = getString(message('stats:pca:FinalDisplay',iter,cvgCond,...
        sprintf('%g',dnorm)));
    fprintf('\n%s\n',finalstr);
end

% 在迭代收敛时存储最终数值
if nargout> 4
    final.rmsResid = dnorm;
    final.Recon = bsxfun(@plus,L*R',mu);
    final.NumIter = iter;
end


%--- 更新函数(Updating Functions) ---
    function L = localL(R,mu)
        %LOCALL假设R固定不变，预测出最小方差的L
		
        L = zeros(n,k); %初始化
        for i = 1:n
            idx = ~idxMiss(i,:);
            L(i,:) = locallscov(R(idx,:),(Y(i,idx)-mu(idx))',Phi(idx))';
			%该函数用于计算：L(i,:)=[(R(idx,:))'R(idx,:)]\[(R(idx,:))'(Y(i,idx)-mu(idx))']，Y(i,idx)-mu(idx)目的是对Y中心化
        end
    end % localL结束

    function R = localR(L,mu)
        %LocalR假设L固定不变，预测出最小方差的R
        
        R = zeros(p,k); %初始化
        for j = 1:p
            idx = ~idxMiss(:,j);
            R(j,:) = locallscov(L(idx,:),Y(idx,j)-mu(j),Omega(idx));
			%该函数用于计算：R(j,:)=[(L(idx,:))'L(idx,:)]\[(L(idx,:))'(Y(idx,j)-mu(j))]，Y(idx,j)-mu(j)目的是对Y中心化
        end
    end % localR结束

    function mu = localMu (L,R)
        
        
        diff = Y-L*R';%返回的是Y和预测矩阵L*R'之间的差分矩阵
        if ~flagCentered%如果输入数据Y未经过中心化
            mu = zeros(1,p);
        else
            mu = classreg.learning.internal.wnanmean(diff,Omega);%对diff矩阵的行空间以Omega为权重向量进行加权
        end
    end % localMu结束

end % 主函数结束

%---------子函数(Sub Function)----------
function x = locallscov(A,B,w)
%加权最小二乘，输入的w必须是一个列向量
%该函数用于计算：x=(A'A)\(A'B)，比文档中少lambda那一个正则化项
C=(bsxfun(@times,A,w)'*A);
if condest(C) > 1/sqrt(eps(class(C)))%condest(C)返回的是C这个矩阵的非正常的度量，值越大说明矩阵越不正常
    x = pinv(C)*(bsxfun(@times,A,w)'*B);%计算输出值x
else
    x = C\(bsxfun(@times,A,w)'*B);%计算输出值x
    % x = lscov(A,B,w);
end
end% locallscov结束